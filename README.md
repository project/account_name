# Account Name

## Table of contents

- Introduction
- Requirements
- Installation
- Configuration
- Maintainers


## Introduction

You can change my account link and show the user profile picture.


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

Goto: /admin/config/user-interface/account-name.


## Maintainers

- Deepak Bhati (heni_deepak) - https://www.drupal.org/u/heni_deepak
- Radheshyam Kumawat (radheymkumar) - https://www.drupal.org/u/radheymkumar
