<?php

namespace Drupal\account_name\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Symfony\Component\DependencyInjection\ContainerInterface;


/**
 * Summary of AccountNameSettingsForm.
 */
class AccountNameSettingsForm extends ConfigFormBase {

  /**
   * Drupal\Core\Entity\EntityTypeManager definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $enityTypeManager;

  /**
   * @param \Drupal\Core\Entity\EntityTypeManager $enityTypeManager
   *   EntityTypeManager objects.
   */

  public function __construct(EntityTypeManager $enityTypeManager) {
    $this->enityTypeManager = $enityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * Summary of getFormId
   * @return string
   */
  public function getFormId() {
    return 'account_name_settings_form';
  }

  /**
   * Summary of getEditableConfigNames
   * @return array
   */
  protected function getEditableConfigNames() {
    return ['account_name.settings'];
  }

  /**
   * Summary of buildForm
   * @param array $form
   * @param FormStateInterface $form_state
   * @return mixed
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $settings = $this->config('account_name.settings');

    // General account_name form settings.
    $form['account_name_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Account Name'),
      '#open' => TRUE,
    ];

    $form['account_name_settings']['enable'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable'),
      '#description' => $this->t('Enable account name settings.'),
      '#default_value' => $settings->get('enable'),
    ];

    $form['account_name_settings']['flip'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Flip position'),
      '#description' => $this->t('filp image and username.'),
      '#default_value' => $settings->get('flip'),
    ];

    $form['account_name_settings']['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#default_value' => $settings->get('label'),
      '#description' => $this->t('Add account name label. Eg. Welcome, Hi, Hello.'),
    ];

    // Image array.
    $image_style = [];
    // Load media image style.
    $styles = $this->enityTypeManager->getStorage('image_style')->loadMultiple();

    foreach ($styles as $key => $value) {
      $image_style[$value->getName()] = $value->label();
    }

    $form['account_name_settings']['image_style'] = [
      '#type' => 'select',
      '#title' => $this->t('Image style'),
      '#options' => $image_style,
      '#default_value' => $settings->get('image_style'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * Summary of validateForm
   * @param array $form
   * @param FormStateInterface $form_state
   * @return void
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * Summary of submitForm
   * @param array $form
   * @param FormStateInterface $form_state
   * @return void
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('account_name.settings')
      ->set('enable', $form_state->getValue('enable'))
      ->set('flip', $form_state->getValue('flip'))
      ->set('label', $form_state->getValue('label'))
      ->set('image_style', $form_state->getValue('image_style'))
      ->save();
    $this->messenger()->addStatus(t('Save account name configuration.'));
  }

}
